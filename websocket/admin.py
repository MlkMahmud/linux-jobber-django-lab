from django.contrib import admin

from .models import (
  ChatMessage,
  Connection,
)

admin.site.register(ChatMessage)
admin.site.register(Connection)
