import json

from boto3 import client
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import ChatMessage, Connection

ENDPOINT_URL = 'https://af7n4hhosa.execute-api.us-east-2.amazonaws.com/test/'

def _parse_body(body):
  body_unicode = body.decode('utf-8')
  return json.loads(body_unicode)


def _send_to(connection_id, data):
  gatewayapi = client('apigatewaymanagementapi', endpoint_url=ENDPOINT_URL)
  Data = json.dumps(data).encode('utf-8')
  return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=Data)


@csrf_exempt
def test(request):
  return JsonResponse({'message': 'hello Daud'}, status=200)


@csrf_exempt
def connect(request):
  body = _parse_body(request.body)
  connection_id = body['connectionId']
  Connection.objects.create(connection_id=connection_id)
  return JsonResponse({'message': 'connected successfully'}, status=200)


@csrf_exempt
def disconnect(request):
  body = _parse_body(request.body)
  connection_id = body['connectionId']
  connection = Connection.objects.get(connection_id=connection_id)
  connection.delete()
  return JsonResponse({'message': 'disconnected successfully'}, status=200)


@csrf_exempt
def send_message(request):
  body = _parse_body(request.body)
  client = Connection.objects.get(connection_id=body['connectionId'])
  ChatMessage.objects.create(
    username=body['username'],
    message=body['message'],
    timestamp=body['timestamp'],
    client=client
  )
  connections = [_.connection_id for _ in Connection.objects.all()]
  data = {'messages': [ body ] }
  for connection in connections:
    _send_to(connection, data)
  
  return JsonResponse({ 'message': 'successfully sent' }, status=200)


@csrf_exempt
def recent_messages(request):
  body = _parse_body(request.body)
  connection_id = body['connectionId']
  messages = []
  for message in ChatMessage.objects.all():
    messages.append(
      {
        'username': message.username,
        'message': message.message,
        'timestamp': message.timestamp
      }
    )
  messages.reverse()
  data = { 'messages': messages }
  _send_to(connection_id, data)

  return JsonResponse({'message': 'success'}, status=200)
