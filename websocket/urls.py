from django.urls import path

from .views import (
  connect,
  disconnect,
  recent_messages,
  send_message,
  test,
)


urlpatterns = [
  path('connect/', connect, name='connect'),
  path('disconnect/', disconnect, name='disconnect'),
  path('recent_messages/', recent_messages),
  path('send_message/', send_message),
  path('test/', test, name='test'),
]
