import setuptools

setuptools.setup(
  name="almalikmahmudscrumy",
  version="0.0.1",
  author="Malik Mahmud",
  author_email="almalikmahmud@gmail.com",
  url="https://gitlab.com/MlkMahmud/linux-jobber-django-lab",
  description="A scrum application",
  packages=setuptools.find_packages(),
  classifiers = [
    'License :: OSI Approved :: BSD 3-Clause "New" or "Revised" License (BSD-3-Clause)'
  ]
)

