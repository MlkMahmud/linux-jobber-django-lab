from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .forms import SignUpForm, CreateGoalForm, EditGoalForm
from .helpers import generate_id
from .models import ScrumyGoals, ScrumyHistory, GoalStatus


def index(request):
  if request.method == 'POST':
    form = SignUpForm(request.POST)
    new_user = form.save(commit=False)
    new_user.set_password(new_user.password)
    new_user.save()
    devs = Group.objects.get(name='Developer')
    devs.user_set.add(new_user)
    return HttpResponse('Your account has been created successfully<br /> <a href="/almalikmahmudscrumy/accounts/login">Login</a>')

  form = SignUpForm()
  return render(request, 'almalikmahmudscrumy/index.html', { 'form': form })

  
@login_required
def move_goal(request, id):
  try:
    goal = ScrumyGoals.objects.get(pk=id)

  except ScrumyGoals.DoesNotExist:
    context = { 'error': 'A record with that goal id does not exist' }
    return render(request, 'almalikmahmudscrumy/exception.html', context)

  else:
    current_user = request.user
    group = current_user.groups.all()[0].name
    user = {
      'current_user': current_user,
      'group': group
    }
    if request.method == 'POST':
      form = EditGoalForm(request.POST, instance=goal)
      goal = form.save(commit=False)
      goal_status = goal.goal_status.status_name
      user = goal.user
      if group == 'Developer' and (goal_status == 'Done Goal' or current_user != user):
        ctx = { 'error': 'Developers can only edit their own goals' }
        return render(request, 'almalikmahmudscrumy/exception.html', ctx)
      
      elif group == 'Quality Assurance':
        ctx = {}
        if current_user == user and goal_status == 'Weekly Goal':
          ctx['error'] = 'QA members can only move goals between daily, verify and done columns.'
          return render(request, 'almalikmahmudscrumy/exception.html', ctx)

        elif current_user != user and goal_status != 'Done Goal':
          ctx['error'] = 'QA members can only move other people\'s goals to the done column.'
          return render(request, 'almalikmahmudscrumy/exception.html', ctx)

      elif group == 'Owner' and user != current_user:
        ctx = { 'error': 'Members of the Owner group can only edit their own goals' }
        return render(request, 'almalikmahmudscrumy/exception.html', ctx)

      goal.save()
      return redirect('home')

    form = EditGoalForm(instance=goal)
    return render(request, 'almalikmahmudscrumy/movegoal.html', { 'form': form, 'user': user })


@login_required
def add_goal(request):
  current_user = request.user
  group = current_user.groups.all()[0].name
  user = {
    'current_user': current_user,
    'group' : group
  }  
  if request.method == 'POST':
    form = CreateGoalForm(request.POST)
    goal = form.save(commit=False)
    goal_status = goal.goal_status.status_name
    username = goal.user
    if (group in ['Developer', 'Quality Assurance']) and (goal_status != 'Weekly Goal' or username != current_user):
      ctx = { 'error': f'Members of the {group} group can only create weekly goals for themselves' }
      return render(request, 'almalikmahmudscrumy/exception.html', ctx)

    elif group == 'Owner' and goal_status != 'Weekly Goal':
      ctx = { 'error': 'Members of the Owner group can only create weekly goals' }
      return render(request, 'almalikmahmudscrumy/exception.html', ctx)

    ids = [goal.goal_id for goal in ScrumyGoals.objects.all()]
    goal.goal_id = generate_id(ids)
    goal.save()
    return redirect('home')

  form = CreateGoalForm()
  return render(request, 'almalikmahmudscrumy/addgoal.html', { 'form': form, 'user': user })


@login_required
def home(request):
  current_user = {
    'current_user': request.user,
    'group': request.user.groups.all()[0].name
  }
  
  users = []
  for user in User.objects.all():
    weekly_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Weekly Goal'))
    daily_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Daily Goal'))
    verify_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Verify Goal'))
    done_goals = user.goals.filter(goal_status=GoalStatus.objects.get(status_name='Done Goal'))
    users.append({
      'username': user.username,
      'weekly_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in weekly_goals]),
      'daily_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in daily_goals]),
      'verify_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in verify_goals]),
      'done_goals': "<br />".join([f"<a href='movegoal/{goal.id}'>{goal.goal_id} - {goal.goal_name}</a>" for goal in done_goals]),
    })

  return render(request, 'almalikmahmudscrumy/home.html', {'users': users, 'user': current_user })
