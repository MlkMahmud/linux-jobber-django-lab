from django.urls import path, include

from .views import index, move_goal, add_goal, home

urlpatterns = [
  path('', index, name="about"),
  path('movegoal/<int:id>', move_goal, name='move_goal'),
  path('addgoal', add_goal, name='add_goal'),
  path('home', home, name='home'),
  path('accounts/', include('django.contrib.auth.urls'))
]


