
from django.db import models
from django.contrib.auth.models import User

class GoalStatus(models.Model):
  def __str__(self):
    return self.status_name
  status_name = models.CharField(max_length=100)

class ScrumyGoals(models.Model):
  def __str__(self):
    return self.goal_name

  user = models.ForeignKey(User, related_name='goals', on_delete=models.PROTECT)
  goal_id = models.IntegerField()
  goal_name = models.CharField(max_length=100)
  goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
  created_by = models.CharField(max_length=100, default='')
  moved_by = models.CharField(max_length=100, default='')
  owner = models.CharField(max_length=100, default='')

class ScrumyHistory(models.Model):
  def __str__(self):
    return self.created_by

  goal = models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)
  moved_by = models.CharField(max_length=100)
  created_by = models.CharField(max_length=100)
  moved_from = models.CharField(max_length=100)
  moved_to = models.CharField(max_length=100)
  time_of_action = models.DateField()
