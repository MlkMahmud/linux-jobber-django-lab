from django.forms import ModelForm, PasswordInput
from django.contrib.auth.models import User

from .models import ScrumyGoals

class SignUpForm(ModelForm):
  class Meta:
    model = User
    fields = ['first_name', 'last_name', 'email', 'username', 'password']
    widgets = {
      'password': PasswordInput(),
    }


class CreateGoalForm(ModelForm):
  class Meta:
    model = ScrumyGoals
    fields = ['goal_name', 'user', 'goal_status']


class EditGoalForm(ModelForm):
  class Meta:
    model = ScrumyGoals
    fields = ['goal_status']
