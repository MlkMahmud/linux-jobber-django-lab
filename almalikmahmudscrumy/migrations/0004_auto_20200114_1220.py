# Generated by Django 3.0.2 on 2020-01-14 12:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('almalikmahmudscrumy', '0003_auto_20200114_1032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scrumygoals',
            name='goal_status',
        ),
        migrations.RemoveField(
            model_name='scrumygoals',
            name='user',
        ),
        migrations.RemoveField(
            model_name='scrumyhistory',
            name='goal',
        ),
        migrations.DeleteModel(
            name='GoalStatus',
        ),
        migrations.DeleteModel(
            name='ScrumyGoals',
        ),
        migrations.DeleteModel(
            name='ScrumyHistory',
        ),
    ]
