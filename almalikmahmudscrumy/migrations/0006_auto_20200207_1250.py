# Generated by Django 3.0.2 on 2020-02-07 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('almalikmahmudscrumy', '0005_goalstatus_scrumygoals_scrumyhistory'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='created_by',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='scrumygoals',
            name='moved_by',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='scrumygoals',
            name='owner',
            field=models.CharField(default='', max_length=100),
        ),
    ]
