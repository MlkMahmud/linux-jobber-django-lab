from random import randint


def generate_id(collection):
  random_id = randint(1000, 9999)

  while random_id in collection:
    random_id = randint(1000, 9999)
  
  return random_id
  
